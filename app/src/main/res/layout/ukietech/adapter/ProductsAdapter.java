package com.example.ukietech.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ukietech.R;
import com.example.ukietech.model.Product;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProductsAdapter extends RecyclerView.Adapter<com.example.ukietech.adapter.ProductsAdapter.ViewHolder> {

    private List<Product> listProducts;

    public ProductsAdapter(List<Product> listProducts) {

        this.listProducts = getListShop();
        sortList(listProducts);
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(listProducts.size()!=0){
            Picasso.get().load(R.mipmap.ic_launcher).into(holder.imgProduct);
//            Glide.with(holder.imgProduct.getContext())
//                    .load(R.mipmap.ic_launcher)
//                    .centerCrop()
//                    .into( holder.imgProduct);
            holder.txNameProduct.setText(listProducts.get(position).getNameProduct());
            holder.txPriceProduct.setText(listProducts.get(position).getPriceProduct());
            holder.txQuantity.setText(String.valueOf(listProducts.get(position).getQuantity()));
        }
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(getItemCount() - 1 - position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(getItemCount() - 1 - position);
    }

    @Override
    public int getItemCount() {
        return listProducts == null ? 0 : listProducts.size();
    }

    public void addNewProduct(com.example.ukietech.model.Product product){
        listProducts.add(0,product);
        notifyDataSetChanged();
    }

    private void sortList(List listProducts){
        Collections.sort(listProducts, new Comparator<Product>() {
            @Override
            public int compare(com.example.ukietech.model.Product o1, com.example.ukietech.model.Product o2) {
                return listProducts.indexOf(o2)- listProducts.indexOf(o1);
            }
        });
    }

    private List<Product> getListShop(){
        listProducts = new ArrayList<>();
        DatabaseReference feedReference = FirebaseDatabase.getInstance().getReference().child("Products").orderByChild("Products").getRef();
        feedReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listProducts.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    com.example.ukietech.model.Product product = snapshot.getValue(com.example.ukietech.model.Product.class);
                    listProducts.add(product);
                    notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        return listProducts;
    }


    class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView imgProduct;
        private TextView txNameProduct;
        private TextView txQuantity;
        private TextView txPriceProduct;

        ViewHolder(View itemView) {
            super(itemView);
            imgProduct = itemView.findViewById(R.id.imageProduct);
            txNameProduct = itemView.findViewById(R.id.textNameProduct);
            txQuantity = itemView.findViewById(R.id.textQuantity);
            txPriceProduct = itemView.findViewById(R.id.textPriceProduct);
        }
        public void setIvContent(String url){
            if (imgProduct == null)return;


        }
    }
}
