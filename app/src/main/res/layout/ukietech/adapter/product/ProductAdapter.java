package com.example.ukietech.adapter.product;

import android.support.annotation.NonNull;
import android.util.Log;
import android.view.ViewGroup;

import com.example.ukietech.model.Product;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

@EBean
public class ProductAdapter extends com.example.ukietech.adapter.core.RecyclerViewAdapterBase<com.example.ukietech.model.Product, com.example.ukietech.adapter.product.ProductItemView> {

    @RootContext
    com.example.ukietech.ui.activity.core.BaseActivity baseActivity;

    @Bean
    com.example.ukietech.adapter.bean.finder.ProductBeanFinder productBeanFinder;

    @AfterViews
    public void initAdapter() {
        DatabaseReference feedReference = FirebaseDatabase.getInstance().getReference().child("Products").orderByChild("Products").getRef();
        feedReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Product> listProduct = new ArrayList<>();
                listProduct.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    com.example.ukietech.model.Product product = snapshot.getValue(com.example.ukietech.model.Product.class);
                    listProduct.add(product);
                    Log.d("product", product.getNameProduct());
                }
                productBeanFinder.sortList(listProduct);
                setItem(listProduct);
                notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }



    @Override
    protected com.example.ukietech.adapter.product.ProductItemView onCreateItemHolder(ViewGroup parent, int viewType) {
        return ProductItemView_.build(baseActivity);
    }

    @Override
    public void onBindViewHolder(@NonNull com.example.ukietech.adapter.core.ViewWrapper<com.example.ukietech.adapter.product.ProductItemView> holder, int position) {
        com.example.ukietech.adapter.product.ProductItemView view = holder.getView();
        com.example.ukietech.model.Product product = getItem().get(position);
        view.setTag(product);
        view.bind(product);
    }
}