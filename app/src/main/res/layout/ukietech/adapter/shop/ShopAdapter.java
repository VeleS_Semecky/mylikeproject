package com.example.ukietech.adapter.shop;

import android.support.annotation.NonNull;
import android.util.Log;
import android.view.ViewGroup;

import com.example.ukietech.model.Shop;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

@EBean
public class ShopAdapter extends com.example.ukietech.adapter.core.RecyclerViewAdapterBase<Shop, com.example.ukietech.adapter.shop.ShopItemView> {

    @RootContext
    com.example.ukietech.ui.activity.core.BaseActivity baseActivity;



    @AfterViews
    public void initAdapter(){
        DatabaseReference feedReference = FirebaseDatabase.getInstance().getReference().child("Shops").orderByChild("Shops").getRef();
        feedReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Shop> listShops = new ArrayList<>();
                listShops.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Shop shop = snapshot.getValue(Shop.class);
                    listShops.add(shop);
                    Log.d("asdasd", shop.getNameShop());
                }
                setItem(listShops);
                notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @Override
    protected com.example.ukietech.adapter.shop.ShopItemView onCreateItemHolder(ViewGroup parent, int viewType) {
        return ShopItemView_.build(baseActivity);
    }

    @Override
    public void onBindViewHolder(@NonNull com.example.ukietech.adapter.core.ViewWrapper<com.example.ukietech.adapter.shop.ShopItemView> holder, int position) {
        com.example.ukietech.adapter.shop.ShopItemView view = holder.getView();
        Shop shop = getItem().get(position);
        view.setTag(shop);
        view.bind(shop);
    }

}
