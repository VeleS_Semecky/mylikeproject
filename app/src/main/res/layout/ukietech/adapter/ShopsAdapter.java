package com.example.ukietech.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ukietech.R;
import com.example.ukietech.model.Shop;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ShopsAdapter extends RecyclerView.Adapter<com.example.ukietech.adapter.ShopsAdapter.ViewHolder>{
    public static final String SHOPS_ROOT = "Shops";
    private List<Shop> listShops;
//    private OnClickItemAdapterListener onClickItemAdapterListener;
//    public ShopsAdapter(List<Shop> listShops, OnClickItemAdapterListener onClickItemAdapterListener) {
//        this.listShops = getListShop();
//        this.onClickItemAdapterListener = onClickItemAdapterListener;
//    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shop,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(listShops.size()!=0){
            holder.textShop.setText(listShops.get(position).getNameShop());
        }
    }

    private List<Shop> getListShop(){
        listShops = new ArrayList<>();
        DatabaseReference feedReference = FirebaseDatabase.getInstance().getReference().child(SHOPS_ROOT).orderByChild(SHOPS_ROOT).getRef();
        feedReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listShops.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Shop shop = snapshot.getValue(Shop.class);
                    listShops.add(shop);
                    notifyDataSetChanged();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        return listShops;
    }


    @Override
    public int getItemCount() {
        return listShops == null ? 0 : listShops.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener,View.OnClickListener{

        private TextView textShop;
        ViewHolder(View itemView) {
            super(itemView);
            textShop = itemView.findViewById(R.id.item_shop);
            textShop.setOnClickListener(this);
            textShop.setOnLongClickListener(this);

        }


        @Override
        public void onClick(View v) {
//            if (onClickItemAdapterListener != null){
//                onClickItemAdapterListener.onClickItemAdapter(v,getAdapterPosition());
//            }
        }

        @Override
        public boolean onLongClick(View v) {
//            if (onClickItemAdapterListener != null){
//                onClickItemAdapterListener.onLongClickItemAdapter(v,getAdapterPosition());
//            }
            return false;
        }

    }
}
