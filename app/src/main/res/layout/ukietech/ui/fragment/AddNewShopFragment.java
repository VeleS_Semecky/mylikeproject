package com.example.ukietech.ui.fragment;

import android.widget.EditText;
import android.widget.Toast;

import com.example.ukietech.R;
import com.example.ukietech.model.Shop;
import com.example.ukietech.ui.fragment.core.BaseFragment;
import com.example.ukietech.ui.navigator.core.ResourceManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Юрий on 26.03.2018.
 */
@EFragment(R.layout.fragment_add_shop_bd)
public class AddNewShopFragment extends BaseFragment {

    @ViewById(R.id.shop_name)
    EditText editText;

    @AfterViews
    public void initToolbar(){
        navigatorManager.getMainManager(this).initToolbar(ResourceManager.ToolbarId.YES_OR_NO, String.valueOf(R.string.app_name));
    }

    @Override
    public void clickToolbar(int id) {
        switch (id) {
            case R.id.cancel_imgv:
                Toast.makeText(getBaseActivity(), "}{yi", Toast.LENGTH_SHORT).show();
            break;
            case R.id.done_imgv:
                addNewShop(editText.getText().toString());
                moveTo();
                Toast.makeText(getBaseActivity(), "asdasdasd", Toast.LENGTH_SHORT).show();
            break;
        }
    }

    void addNewShop(String s){
        Shop shop = new Shop(s);
        DatabaseReference myRef;
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String SHOPS_ROOT = "Shops";
        myRef = FirebaseDatabase.getInstance().getReference().child(SHOPS_ROOT);
        String key = myRef.push().getKey();
        myRef.child(key).setValue(shop);
    }

    void moveTo(){
        navigatorManager.getMainManager(this).removeFragment();
        navigatorManager.getMainManager(this).moveFragmentTo(ResourceManager.FragmentId.LIST_SHOPS_FRAGMENT);
    }

}
