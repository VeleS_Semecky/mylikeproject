package com.example.ukietech.ui.navigator.core;



import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * Created by Юрий on 24.03.2018.
 */
@EBean
public abstract class NavigatorBaseManager {

    @RootContext
    protected com.example.ukietech.ui.activity.core.BaseActivity baseActivity;

    public abstract com.example.ukietech.ui.navigator.core.Manager getMainManager(com.example.ukietech.ui.fragment.core.BaseFragment baseFragment);

}
