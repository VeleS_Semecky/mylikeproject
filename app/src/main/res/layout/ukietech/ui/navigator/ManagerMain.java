package com.example.ukietech.ui.navigator;


import android.support.annotation.NonNull;

import com.example.ukietech.R;
import com.example.ukietech.ui.fragment.AddNewShopFragment_;
import com.example.ukietech.ui.fragment.AuthContainerFragment_;
import com.example.ukietech.ui.fragment.FirstFragment_;
import com.example.ukietech.ui.fragment.FragmentToolbarButContentNull_;
import com.example.ukietech.ui.fragment.ListProductsFragment_;
import com.example.ukietech.ui.fragment.ListShopsFragment_;

import org.androidannotations.api.builder.FragmentBuilder;

/**
 * Created by Юрий on 24.03.2018.
 */

public class ManagerMain extends com.example.ukietech.ui.navigator.core.BaseManager {

    ManagerMain(com.example.ukietech.ui.fragment.core.BaseFragment baseFragment, com.example.ukietech.ui.activity.core.BaseActivity baseActivity) {
        super(baseFragment, baseActivity);
    }

    @Override
    public void moveFragmentTo(int id, Object... o) {
        switch (id){
            case com.example.ukietech.ui.navigator.core.ResourceManager.FragmentId.FIRST_FRAGMENT:
                baseActivity.getFragmentManager().beginTransaction().replace(R.id.fragment_container, FirstFragment_.builder().build(), com.example.ukietech.ui.navigator.core.ResourceNames.FIRST_FRAGMENT).commit();
                break;
            case com.example.ukietech.ui.navigator.core.ResourceManager.FragmentId.AUTH_FRAGMENT:
                baseActivity.getFragmentManager().beginTransaction().replace(R.id.containerAuth, AuthContainerFragment_.builder().build(), com.example.ukietech.ui.navigator.core.ResourceNames.AUTH_FRAGMENT).commit();
                break;
            case com.example.ukietech.ui.navigator.core.ResourceManager.FragmentId.NULL_FRAGMENT:
                baseActivity.getFragmentManager().beginTransaction().replace(R.id.containerToolbar, FragmentToolbarButContentNull_.builder().build(), com.example.ukietech.ui.navigator.core.ResourceNames.NULL_FRAGMENT).commit();
                break;
            case com.example.ukietech.ui.navigator.core.ResourceManager.FragmentId.ADD_NEW_SHOP_FRAGMENT:
                baseActivity.getFragmentManager().beginTransaction().replace(R.id.fragment_container, AddNewShopFragment_.builder().build(), com.example.ukietech.ui.navigator.core.ResourceNames.ADD_NEW_SHOP_FRAGMENT).commit();
                break;
            case com.example.ukietech.ui.navigator.core.ResourceManager.FragmentId.LIST_SHOPS_FRAGMENT:
                baseActivity.getFragmentManager().beginTransaction().replace(R.id.fragment_container, ListShopsFragment_.builder().build(), com.example.ukietech.ui.navigator.core.ResourceNames.LIST_SHOPS_FRAGMENT).commit();
                break;
            case com.example.ukietech.ui.navigator.core.ResourceManager.FragmentId.LIST_PRODUCT_FRAGMENT:
                replaceFragment(R.id.fragment_container, ListProductsFragment_.builder(),o[0].toString(), com.example.ukietech.ui.navigator.core.ResourceNames.LIST_PRODUCTS_FRAGMENT);
                break;

        }
    }

    private void replaceFragment(int layoutFragment, FragmentBuilder fragmentBuilder, @NonNull String bundle, String resourceName){
        baseActivity.getFragmentManager().beginTransaction().replace(layoutFragment, (android.app.Fragment) fragmentBuilder.arg("bundle",bundle).build(), resourceName).commit();
    }

    private void replaceFragment(int layoutFragment, FragmentBuilder fragmentBuilder, int bundle, String resourceName){
        baseActivity.getFragmentManager().beginTransaction().replace(layoutFragment, (android.app.Fragment) fragmentBuilder.arg("bundle",bundle).build(), resourceName).commit();
    }

    @Override
    public void removeFragment() {
            baseFragment.getActivity().getFragmentManager().beginTransaction().remove(baseFragment).commit();
    }
}
