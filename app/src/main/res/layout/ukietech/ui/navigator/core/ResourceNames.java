package com.example.ukietech.ui.navigator.core;

/**
 * Created by Юрий on 24.03.2018.
 */

public class ResourceNames {

    public static final String FIRST_FRAGMENT = "FirstFragment";
    public static final String AUTH_FRAGMENT = "AuthContainerFragment";
    public static final String NULL_FRAGMENT = "FragmentToolbarButContentNull";
    public static final String LIST_PRODUCTS_FRAGMENT = "ListProductsFragment";
    public static final String LIST_SHOPS_FRAGMENT = "ListShopsFragment";
    public static final String ADD_NEW_SHOP_FRAGMENT = "AddNewShopFragment";


}
