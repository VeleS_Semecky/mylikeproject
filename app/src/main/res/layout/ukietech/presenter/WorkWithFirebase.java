package com.example.ukietech.presenter;

import com.example.ukietech.presenter.core.MvpPresenter;
import com.example.ukietech.presenter.core.MvpView;

public interface WorkWithFirebase {
        interface View extends MvpView {
            void showAddNewShop();
            void showGoodAddInFirebase();
            void showFailedAddInFirebase();
        }

    interface Presenter extends MvpPresenter<View> {

        // field is filled
        void onAddNewShop();
        void onFailedAdd();
        void onGoodAdd();
    }


}
