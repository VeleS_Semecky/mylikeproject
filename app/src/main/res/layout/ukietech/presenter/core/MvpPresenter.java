package com.example.ukietech.presenter.core;

public interface MvpPresenter<V extends com.example.ukietech.presenter.core.MvpView> {

    void attachView(V mvpView);

    void viewIsReady();

    void detachView();

    void destroy();
}