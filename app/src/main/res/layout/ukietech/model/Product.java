package com.example.ukietech.model;

/**
 * Created by Юрий on 26.03.2018.
 */

public class Product {
    private String urlPhoto;
    private String nameProduct;
    private String priceProduct;
    private int quantity;

    public Product() {
    }

    public Product(String urlPhoto, String nameProduct, String priceProduct, int quantity) {
        this.urlPhoto = urlPhoto;
        this.nameProduct = nameProduct;
        this.priceProduct = priceProduct;
        this.quantity = quantity;
    }

    public String getUrlPhoto() {
        return urlPhoto;
    }

    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public String getPriceProduct() {
        return priceProduct;
    }

    public void setPriceProduct(String priceProduct) {
        this.priceProduct = priceProduct;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
