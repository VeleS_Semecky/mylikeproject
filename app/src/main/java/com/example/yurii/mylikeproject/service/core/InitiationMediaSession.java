package com.example.yurii.mylikeproject.service.core;

import android.content.Context;

public interface InitiationMediaSession {
    void onCreateMediaSession(MyMediaSession myMediaSession, Context context, MyMusicService baseMusicService);
    void onDestroyMediaSession();
}
