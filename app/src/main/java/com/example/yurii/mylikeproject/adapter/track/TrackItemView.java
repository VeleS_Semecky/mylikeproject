package com.example.yurii.mylikeproject.adapter.track;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yurii.mylikeproject.R;
import com.example.yurii.mylikeproject.adapter.bean.finder.TrackBeanFinder;
import com.example.yurii.mylikeproject.adapter.bean.finder.TrackExoFinder;
import com.example.yurii.mylikeproject.model.Track;
import com.example.yurii.mylikeproject.model.TrackStart;
import com.example.yurii.mylikeproject.service.core.ServiceViewModel;
import com.example.yurii.mylikeproject.ui.activity.core.BaseActivity;
import com.example.yurii.mylikeproject.ui.navigator.NavigatorManager;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.LongClick;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.ViewById;

@EViewGroup(R.layout.item_tack_in_list)
public class TrackItemView extends LinearLayout {
    @Bean
    protected NavigatorManager navigatorManager;

    ServiceViewModel mServiceViewModel;
//    TrackExoFinder trackExoFinder;

    @Bean
    TrackBeanFinder trackBeanFinder;
    @ViewById(R.id.trArtist)
    TextView trArtist;
    @ViewById(R.id.trTitle)
    TextView trTitle;
    @ViewById(R.id.trLong)
    TextView trLong;
    @ViewById(R.id.trImg)
    ImageView trImg;



    @AfterViews
    public void init(){
    }

    @Click(R.id.itemTrackInList)
    public void OnClick(){
     //  Toast.makeText(getContext(),getTag().toString(),Toast.LENGTH_SHORT).show();
        mServiceViewModel.playMusic((Track) getTag());
//        trackExoFinder.playMusic((Track) getTag());
//        trackExoFinder.playMusic((trackBeanFinder.getTrack().get(0)));
        //navigatorManager.getMainManager(null).moveFragmentTo(com.example.ukietech.ui.navigator.core.ResourceManager.FragmentId.LIST_PRODUCT_FRAGMENT, item_shop.getText());
    }
//    @LongClick(R.id.itemTrackInList)
//    public void OnLongClick(){
//        trackExoFinder.seekTo(null);
//    }

    public TrackItemView(Context context) {
        super(context);
        this.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        mServiceViewModel = ViewModelProviders.of((FragmentActivity) context).get(ServiceViewModel.class);
    }

    public void bind(Track track){
        trArtist.setText( track.getArtist());
        trTitle.setText( track.getTitle());
        trLong.setText( track.getCorrectDuration(track.getDuration()));
        if(track.getUri().trim().isEmpty()) {
            trImg.setImageURI(Uri.parse(track.getUri()));
        }
    }


//    public void setPlayerInItem(TrackExoFinder playerInItem) {
//        this.trackExoFinder = playerInItem;
//    }
}
