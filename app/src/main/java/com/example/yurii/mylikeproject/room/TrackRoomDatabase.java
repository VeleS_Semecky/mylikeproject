package com.example.yurii.mylikeproject.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.yurii.mylikeproject.model.Track;

@Database(entities = {Track.class}, version = 1, exportSchema = false)
public abstract class TrackRoomDatabase extends RoomDatabase {

    public abstract TrackDAO trackDAO();

    private static TrackRoomDatabase INSTANCE;

    public static TrackRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (TrackRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            TrackRoomDatabase.class, "track_database")
                            // Wipes and rebuilds instead of migrating if no Migration object.
                            // Migration is not part of this codelab.
//                            .fallbackToDestructiveMigration()
                            .build();

                }
            }
        }
        return INSTANCE;
    }
}
