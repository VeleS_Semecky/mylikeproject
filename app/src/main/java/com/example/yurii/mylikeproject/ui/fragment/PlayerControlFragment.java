package com.example.yurii.mylikeproject.ui.fragment;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.yurii.mylikeproject.R;
import com.example.yurii.mylikeproject.adapter.bean.finder.TrackExoFinder;
import com.example.yurii.mylikeproject.model.Track;
import com.example.yurii.mylikeproject.room.TrackViewModel;
import com.example.yurii.mylikeproject.service.core.ServiceViewModel;
import com.example.yurii.mylikeproject.ui.activity.core.BaseActivity;
import com.example.yurii.mylikeproject.ui.fragment.core.BaseFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.SeekBarProgressChange;
import org.androidannotations.annotations.ViewById;
import org.w3c.dom.Text;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

@EFragment(R.layout.fragment_player_control)
public class PlayerControlFragment extends BaseFragment {




    List<Track> tracks;
    ServiceViewModel mServiceViewModel;
    @ViewById(R.id.txtMaxTime)
    TextView txtMaxTime;
    @ViewById(R.id.txtNowTime)
    TextView txtNowTime;
    @ViewById(R.id.seekBarMusic)
    SeekBar mSeekBar;
    Timer timer = new Timer();
    Handler h;
    @Click(R.id.controlPlay)
    void onPlay(){
        mServiceViewModel.playMusic(tracks.get(0));
    }
    @Click(R.id.controlPrey)
    void onPrey(){
        mServiceViewModel.playMusic(tracks.get(0));
    }
    @Click(R.id.controlNext)
    void onNext(){
        mServiceViewModel.toNextMusic();
    }

    @SeekBarProgressChange(R.id.seekBarMusic)
    void onProgressChangeOnSeekBar(SeekBar seekBar, int progress, boolean fromUser) {
        seekBar.setMax((int) tracks.get(0).getDuration());
        if(fromUser) {
            mServiceViewModel.seekTo(progress);
        }
    }

    @AfterViews
    void initAdapter(){

        // Get a new or existing ViewModel from the ViewModelProvider.
        mServiceViewModel = ViewModelProviders.of(getBaseActivity()).get(ServiceViewModel.class);

        TrackViewModel mTrackViewModel;
        // Get a new or existing ViewModel from the ViewModelProvider.
        mTrackViewModel = ViewModelProviders.of(getBaseActivity()).get(TrackViewModel.class);

        // Add an observer on the LiveData returned by getAlphabetizedWords.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        mTrackViewModel.getAllTracks().observe(getBaseActivity(), new Observer<List<Track>>() {
            @Override
            public void onChanged(@Nullable final List<Track> words) {
                tracks = words;
                if(!tracks.isEmpty()) {
                    txtMaxTime.setText(getCorrectDuration(tracks.get(0).getDuration()));
                }
            }
        });


        h = new Handler() {
            public void handleMessage(android.os.Message msg) {
                if(mServiceViewModel.isPlaying()) {
                    txtNowTime.setText(getCorrectDuration(msg.what));
                }
            }
        };
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if(mServiceViewModel.isPlaying()) {
                    mSeekBar.setProgress((int) mServiceViewModel.getExoPlayer());
                    h.sendEmptyMessage((int) mServiceViewModel.getExoPlayer());
                }
            }
        },0,100);
        // Something Here

    }

    @Nullable
    public static String getCorrectDuration(long songs_duration) {
        // TODO Auto-generated method stub
        String songDurationString;

        long seconds = songs_duration /1000;
        long minutes = seconds/60;
        seconds = seconds % 60;

        if(seconds<10){
            songDurationString = String.valueOf(minutes) + ":0" + String.valueOf(seconds);
        }else{
            songDurationString = String.valueOf(minutes) + ":" + String.valueOf(seconds);
        }
        return songDurationString;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        timer.cancel();
    }
}
