package com.example.yurii.mylikeproject.adapter.bean.finder;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.media.session.MediaControllerCompat;

import com.example.yurii.mylikeproject.model.Track;
import com.example.yurii.mylikeproject.service.core.MyMusicService;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import static android.content.Context.BIND_AUTO_CREATE;

@EBean
public class TrackExoFinder {
    @RootContext
    Context context;



    public void prepareExoPlayerFromFileUri(String s) {
        String s1 ="/storage/emulated/0/Android/media/com.viber.voip/Notifications/viber_message.mp3";
        Uri uri = Uri.parse(s);
        SimpleExoPlayer exoPlayer = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(context), new DefaultTrackSelector(), new DefaultLoadControl());
        ExtractorMediaSource media = new ExtractorMediaSource.Factory(new DefaultDataSourceFactory(context, Util.getUserAgent(context, ""))).setExtractorsFactory(new DefaultExtractorsFactory()).createMediaSource(uri);
        exoPlayer.prepare(media);
        exoPlayer.setPlayWhenReady(true);

    }



    MyMusicService.MyBinder playerServiceBinder;
    MediaControllerCompat mediaController;
    private ServiceConnection serviceConnection;
    public void startService(){
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                playerServiceBinder = (MyMusicService.MyBinder) service;
                try {
                    mediaController = new MediaControllerCompat(
                            context, playerServiceBinder.getMediaSessionToken());
                } catch (RemoteException e) {
                    e.printStackTrace();
                    mediaController = null;
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                playerServiceBinder = null;
                mediaController = null;
            }
        };
        context.bindService(new Intent(context, MyMusicService.class), serviceConnection, BIND_AUTO_CREATE);
    }
    public void playMusic(Track track){
        if (mediaController != null) {
            playerServiceBinder.getMediaSession().setTrack(track);
            mediaController.getTransportControls().play();
        }
    }

    public void seekTo(Track track){
        if (mediaController != null) {
            mediaController.getTransportControls().seekTo(1000);
        }
    }

    public void stopMusic(){
            if (mediaController != null)
                mediaController.getTransportControls().stop();

//        playerServiceBinder = null;
//        if (mediaController != null) {
////            mediaController.unregisterCallback(callback);
//            mediaController = null;
//        }
//        context.unbindService(serviceConnection);
    }
    public void onDestroyMusicResource(){
//        playerServiceBinder = null;
//        if (mediaController != null) {
//          //  mediaController.unregisterCallback(callback);
//            mediaController = null;
//        }
//        context.unbindService(serviceConnection);
//        Toast.makeText(context,"Destroy",Toast.LENGTH_SHORT).show();

    }




}
