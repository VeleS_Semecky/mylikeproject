package com.example.yurii.mylikeproject.room;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.example.yurii.mylikeproject.model.Track;

import java.util.List;
@Dao
public interface TrackDAO {
    // Добавление Person в бд
//    @Insert
//    void insertAll(Track people);
//    // Добавление Person в бд
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllTrack(List<Track> people);
    // Добавление Person в бд
    // Удаление Person из бд
    @Delete
    void delete(Track person);

    // Получение всех Person из бд
    @Query("SELECT * FROM track_table")
    LiveData<List<Track>> getAllTrack();



//    // Получение всех Person из бд с условием
//    @Query("SELECT * FROM person WHERE favoriteColor LIKE :color")
//    LiveData<List<Track>> getAllPeopleWithFavoriteColor(String color);
}
