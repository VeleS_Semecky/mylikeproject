package com.example.yurii.mylikeproject.service.core;

import android.app.Service;
import android.arch.lifecycle.LifecycleObserver;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaSessionCompat;

import com.example.yurii.mylikeproject.model.Track;

public class MyMusicService extends Service{
    private MyMediaSession myMediaSession = new MyMediaSession();

    @Override
    public void onCreate() {
        super.onCreate();

        myMediaSession.onCreateMediaSession(myMediaSession,getApplicationContext(),this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Ресурсы освобождать обязательно
        myMediaSession.onDestroyMediaSession();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        MediaButtonReceiver.handleIntent(myMediaSession.getMediaSession(), intent);
        return super.onStartCommand(intent, flags, startId);
    }



    public class MyBinder extends Binder {
        public MyMusicService getService() {
            return MyMusicService.this;
        }
        public MediaSessionCompat.Token getMediaSessionToken() {
            return myMediaSession.getMediaSession().getSessionToken();
        }
        public MyMediaSession getMediaSession(){
            if(myMediaSession!=null) {
                return myMediaSession;
            }
            else {
                return null;
            }
        }
    }
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new MyBinder();
    }

    public void setPositionTrack(Track s) {
        myMediaSession.setTrack(s);
    }



}
