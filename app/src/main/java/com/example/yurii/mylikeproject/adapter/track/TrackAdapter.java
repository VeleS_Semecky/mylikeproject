package com.example.yurii.mylikeproject.adapter.track;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import com.example.yurii.mylikeproject.adapter.bean.finder.TrackBeanFinder;
import com.example.yurii.mylikeproject.adapter.bean.finder.TrackExoFinder;
import com.example.yurii.mylikeproject.adapter.core.RecyclerViewAdapterBase;
import com.example.yurii.mylikeproject.adapter.core.ViewWrapper;
import com.example.yurii.mylikeproject.model.Track;
import com.example.yurii.mylikeproject.model.TrackStart;
import com.example.yurii.mylikeproject.room.TrackViewModel;
import com.example.yurii.mylikeproject.service.core.ServiceViewModel;
import com.example.yurii.mylikeproject.ui.activity.core.BaseActivity;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.List;

@EBean
public class TrackAdapter extends RecyclerViewAdapterBase<Track, TrackItemView> {

    @RootContext
    BaseActivity baseActivity;

    @Bean
    TrackBeanFinder trackBeanFinder;

    @Bean
    TrackExoFinder trackExoFinder;


    @AfterViews
    void initAdapter(){
        TrackViewModel mTrackViewModel;
        // Get a new or existing ViewModel from the ViewModelProvider.
        mTrackViewModel = ViewModelProviders.of(baseActivity).get(TrackViewModel.class);
        // Add an observer on the LiveData returned by getAlphabetizedWords.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        mTrackViewModel.getAllTracks().observe(baseActivity, new Observer<List<Track>>() {
            @Override
            public void onChanged(@Nullable final List<Track> words) {
                // Update the cached copy of the words in the adapter.
                //adapter.setWords(words);

            }
        });

        mTrackViewModel.insert(trackBeanFinder.getTrack());
        setItem(trackBeanFinder.getTrack());

//        mTrackViewModel.delete(getValue().get(1));

//        trackExoFinder.startService();
//        trackBeanFinder.prepareExoPlayerFromFileUri("");
    }

    @Override
    protected TrackItemView onCreateItemHolder(ViewGroup parent, int viewType) {
        return TrackItemView_.build(baseActivity);
    }

    @Override
    public void onDestroyMusicResource() {
        trackExoFinder.onDestroyMusicResource();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewWrapper<TrackItemView> holder, int position) {

        TrackItemView view = holder.getView();
        Track track = getItem().get(position);
        view.bind(track);
//        view.setPlayerInItem(trackExoFinder);
        view.setTag(track);
//                view.setOnClickListener(v -> trackExoFinder.playMysic());

//        view.setOnClickListener(v -> { trackBeanFinder.prepareExoPlayerFromFileUri(trackStart.getName());});
//        view.setOnClickListener(v -> trackExoFinder.startMyService(trackStart.getName()));
    }
}
