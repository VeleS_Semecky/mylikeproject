package com.example.yurii.mylikeproject.room;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.AsyncTask;

import com.example.yurii.mylikeproject.model.Track;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class TrackRepository {
    private TrackDAO mTrackDao;
    private LiveData<List<Track>> mAllTracks;

    public TrackRepository(Context application) {
        TrackRoomDatabase db = TrackRoomDatabase.getDatabase(application);
        mTrackDao = db.trackDAO();
        mAllTracks = mTrackDao.getAllTrack();

    }

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    public LiveData<List<Track>> getAllTracks() {
        return mAllTracks;
    }



    // You must call this on a non-UI thread or your app will crash.
    // Like this, Room ensures that you're not doing any long running operations on the main
    // thread, blocking the UI.
    public void insert (List<Track> track) {
        new insertAsyncTask(mTrackDao).execute(track);
    }
    public void delete (Track track) {
        new deleteAsyncTask(mTrackDao).execute(track);
    }


    public static class insertAsyncTask extends AsyncTask<List<Track> , Void, Void> {

        private TrackDAO mAsyncTaskDao;

        insertAsyncTask(TrackDAO dao) {
            mAsyncTaskDao = dao;
        }


        @Override
        protected Void doInBackground(List<Track> ... lists) {
                mAsyncTaskDao.insertAllTrack(lists[0]);

            return null;
        }
    }
    public static class deleteAsyncTask extends AsyncTask<Track , Void, Void> {

        private TrackDAO mAsyncTaskDao;

        deleteAsyncTask(TrackDAO dao) {
            mAsyncTaskDao = dao;
        }


        @Override
        protected Void doInBackground(Track ... lists) {
//            mAsyncTaskDao.insertAllTrack(lists[0]);
            if(lists!=null) {
                mAsyncTaskDao.delete(lists[0]);
            }
            return null;
        }
    }
}
