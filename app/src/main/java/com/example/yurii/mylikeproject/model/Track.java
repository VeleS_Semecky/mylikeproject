package com.example.yurii.mylikeproject.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

@Entity(tableName = "track_table")
public class Track {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "title")
    private String title;
    @ColumnInfo(name = "artist")
    private String artist;
    @ColumnInfo(name = "bitmapResId")
    private int bitmapResId;
    @ColumnInfo(name = "uri")
    private String uri;
    @ColumnInfo(name = "duration")
    private long duration; // in ms



    public Track(String title, String artist, int bitmapResId, String uri, long duration) {
        this.title = title;
        this.artist = artist;
        this.bitmapResId = bitmapResId;
        this.uri = uri;
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public String getArtist() {
        return artist;
    }

    public int getBitmapResId() {
        return bitmapResId;
    }

    public String getUri() {
        return uri;
    }

    public long getDuration() {
        return duration;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setBitmapResId(int bitmapResId) {
        this.bitmapResId = bitmapResId;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }
    @Nullable
    public static String getCorrectDuration(long songs_duration) {
        // TODO Auto-generated method stub
        String songDurationString;

        long seconds = songs_duration /1000;
        long minutes = seconds/60;
        seconds = seconds % 60;

        if(seconds<10){
            songDurationString = String.valueOf(minutes) + ":0" + String.valueOf(seconds);
        }else{
            songDurationString = String.valueOf(minutes) + ":" + String.valueOf(seconds);
        }
        return songDurationString;

    }
}
