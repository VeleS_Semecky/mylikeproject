package com.example.yurii.mylikeproject.adapter.bean;

import android.support.annotation.RequiresPermission;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;


import com.example.yurii.mylikeproject.R;
import com.example.yurii.mylikeproject.adapter.bean.finder.TrackBeanFinder;
import com.example.yurii.mylikeproject.adapter.track.TrackAdapter;
import com.example.yurii.mylikeproject.model.Track;
import com.example.yurii.mylikeproject.ui.activity.core.BaseActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.ViewById;

@EBean
public class ProviderBeanTrack {

    @RootContext
    BaseActivity baseActivity;

    @Bean
    TrackAdapter trackAdapter;


    @ViewById(R.id.recycler_view)
    RecyclerView recyclerView;

    @AfterViews
    public void initTrackAdapter(){
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(baseActivity));
        recyclerView.setAdapter(trackAdapter);
    }

    public void onDestroyMusicResource() {trackAdapter.onDestroyMusicResource(); }
}
