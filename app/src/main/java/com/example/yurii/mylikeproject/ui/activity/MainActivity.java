package com.example.yurii.mylikeproject.ui.activity;

import android.app.Activity;
import android.view.Window;

import com.example.yurii.mylikeproject.R;
import com.example.yurii.mylikeproject.ui.activity.core.BaseActivity;
import com.example.yurii.mylikeproject.ui.navigator.core.ResourceManager;
import com.example.yurii.mylikeproject.ui.navigator.NavigatorManager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.WindowFeature;

@WindowFeature({Window.FEATURE_NO_TITLE})
@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {
    @Bean
    public NavigatorManager navigatorManager;

    @AfterViews
    public void initView(){
        navigatorManager.getMainManager(null).moveFragmentTo(ResourceManager.FragmentId.LIST_TRACK_FRAGMENT);
        navigatorManager.getMainManager(null).moveFragmentTo(ResourceManager.FragmentId.PLAYER_CONTROL_FRAGMENT);
    }
}
