package com.example.yurii.mylikeproject.service.core;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;

import com.example.yurii.mylikeproject.model.Track;

import java.net.URI;

public class MyMediaMetadata {

    private static MediaMetadataCompat.Builder metadataBuilder;

    public static MediaMetadataCompat.Builder dateTrACK(@NonNull Track track){
        if(metadataBuilder == null){
            metadataBuilder = new MediaMetadataCompat.Builder();
        }
        //TODO: Add image
            return metadataBuilder
//                .putBitmap(MediaMetadataCompat.METADATA_KEY_ART, BitmapFactory.decodeResource(getResources(), s.getBitmapResId()))
                    .putString(MediaMetadataCompat.METADATA_KEY_TITLE, track.getTitle())
                    .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, track.getArtist())
                    .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_URI, track.getUri())
                    .putLong(MediaMetadataCompat.METADATA_KEY_DURATION, track.getDuration());
        }

    }
//mediaSession.setMetadata(build());

