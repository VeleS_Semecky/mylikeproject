package com.example.yurii.mylikeproject.service.core;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.widget.Toast;

import com.example.yurii.mylikeproject.model.Track;

import static android.content.Context.BIND_AUTO_CREATE;

public class ServiceViewModel extends AndroidViewModel {
    private MyMusicService.MyBinder playerServiceBinder;
    private MediaControllerCompat mediaController ;
    private ServiceConnection serviceConnection;
    private MediaControllerCompat.Callback callback;
    private boolean playing;

    public boolean isPlaying() {
        return playing;
    }

    public ServiceViewModel(@NonNull Application application) {
        super(application);

        callback = new MediaControllerCompat.Callback() {
            @Override
            public void onPlaybackStateChanged(PlaybackStateCompat state) {
                if (state == null)
                    return;
                playing = state.getState() == PlaybackStateCompat.STATE_PLAYING;
            }
        };
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                Toast.makeText(getApplication(),"onServiceConnected",Toast.LENGTH_SHORT).show();
                playerServiceBinder = (MyMusicService.MyBinder) service;
                try {
                    mediaController = new MediaControllerCompat(
                            getApplication(), playerServiceBinder.getMediaSessionToken());
                    mediaController.registerCallback(callback);
                    callback.onPlaybackStateChanged(mediaController.getPlaybackState());
                } catch (RemoteException e) {
                    e.printStackTrace();
                    mediaController = null;
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                Toast.makeText(getApplication(),"NOonServiceConnected",Toast.LENGTH_SHORT).show();
                playerServiceBinder = null;
                if (mediaController != null) {
                    mediaController.unregisterCallback(callback);
                    mediaController = null;
                }
            }
        };
        application.bindService(new Intent(application, MyMusicService.class), serviceConnection, BIND_AUTO_CREATE);
        if(playing){
            Toast.makeText(getApplication(),"playing",Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(getApplication(),"NOplaying",Toast.LENGTH_SHORT).show();

        }
    }

    public void playMusic(Track track){
        if (mediaController != null) {
            Toast.makeText(getApplication(),"Play",Toast.LENGTH_SHORT).show();
            playerServiceBinder.getMediaSession().setTrack(track);
            mediaController.getTransportControls().play();
        }
        else {
            Toast.makeText(getApplication(), "NoPlay", Toast.LENGTH_SHORT).show();
        }
    }

    public void seekTo(long l){
        if (mediaController != null&&playing) {
            mediaController.getTransportControls().seekTo(l);
        }
    }

    public void stopMusic(){

            mediaController.getTransportControls().stop();

//        playerServiceBinder = null;
//        if (mediaController != null) {
////            mediaController.unregisterCallback(callback);
//            mediaController = null;
//        }
//        context.unbindService(serviceConnection);
    }

    public long getExoPlayer() {
        if(playing) {
            if (mediaController != null) {
                return playerServiceBinder.getMediaSession().getExoPlayer().getCurrentPosition();
            }
            return -1;
        }
        return 0;
    }

    public void toNextMusic(){
        if (mediaController != null&&playing) {
            mediaController.getTransportControls().skipToNext();
        }
    }

    public void toPreyMusic(){
        if (mediaController != null&&playing) {
            mediaController.getTransportControls().skipToPrevious();
        }
    }

    @Override
    protected void onCleared() {
        playing = false;
        playerServiceBinder = null;
        if (mediaController != null) {
            //  mediaController.unregisterCallback(callback);
            mediaController = null;
        }
        getApplication().unbindService(serviceConnection);
        Toast.makeText(getApplication(),"Destroy",Toast.LENGTH_SHORT).show();
        super.onCleared();
    }



}
