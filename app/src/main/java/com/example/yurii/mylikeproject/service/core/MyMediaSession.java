package com.example.yurii.mylikeproject.service.core;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.Observer;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.widget.Toast;

import com.example.yurii.mylikeproject.R;
import com.example.yurii.mylikeproject.model.Track;
import com.example.yurii.mylikeproject.room.TrackRepository;
import com.example.yurii.mylikeproject.ui.activity.MainActivity;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import java.util.List;


public class MyMediaSession extends MediaSessionCompat.Callback implements InitiationMediaSession,AudioManager.OnAudioFocusChangeListener,Player.EventListener{
    private Context context;

    private List<Track> myList;
    private Track track;
    private int INDEX;

    private AudioManager audioManager;

    private MyMusicService baseMusicService;

    private boolean audioFocusRequested = false;
    private AudioFocusRequest audioFocusRequest;

    private final int NOTIFICATION_ID = 404;
    private final String NOTIFICATION_DEFAULT_CHANNEL_ID = "MyMusicChannel";
    private int currentState = PlaybackStateCompat.STATE_STOPPED;

    public void setTrack(Track track) {
        if(this.track!=null) {
            this.onStop();
        }
        this.track = track;
        prepareExoPlayerFromFileUri(track);
    }

    private MediaSessionCompat mediaSession;


    public MediaSessionCompat getMediaSession() {
        return mediaSession;
    }

    private SimpleExoPlayer exoPlayer;

    private final PlaybackStateCompat.Builder stateBuilder = new PlaybackStateCompat.Builder().setActions(
                      PlaybackStateCompat.ACTION_PLAY
                    | PlaybackStateCompat.ACTION_STOP
                    | PlaybackStateCompat.ACTION_PAUSE
                    | PlaybackStateCompat.ACTION_PLAY_PAUSE
                    | PlaybackStateCompat.ACTION_SKIP_TO_NEXT
                    | PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS
    );

    @Override
    public void onPlay() {
        if (!exoPlayer.getPlayWhenReady()) {
            baseMusicService.startService(new Intent(baseMusicService, MyMusicService.class));

            mediaSession.setMetadata(MyMediaMetadata.dateTrACK(track).build());
            //OR
            //dateTrACK(track);

            if (!audioFocusRequested) {
                audioFocusRequested = true;

                int audioFocusResult;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    audioFocusResult = audioManager.requestAudioFocus(audioFocusRequest);
                } else {
                    audioFocusResult = audioManager.requestAudioFocus(audioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
                }
                if (audioFocusResult != AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
                    return;
            }

            mediaSession.setActive(true); // Сразу после получения фокуса

            baseMusicService.registerReceiver(becomingNoisyReceiver, new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY));

            exoPlayer.setPlayWhenReady(true);

        }
        mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_PLAYING, PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1).build());
        currentState = PlaybackStateCompat.STATE_PLAYING;

        NotificationAndForegroundStatus.refreshNotificationAndForegroundStatus(currentState,baseMusicService,getMediaSession());

    }

    @Override
    public void onStop() {
        if (exoPlayer.getPlayWhenReady()) {
            exoPlayer.setPlayWhenReady(false);
            baseMusicService.unregisterReceiver(becomingNoisyReceiver);
        }

        if (audioFocusRequested) {
            audioFocusRequested = false;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                audioManager.abandonAudioFocusRequest(audioFocusRequest);
            } else {
                audioManager.abandonAudioFocus(audioFocusChangeListener);
            }
        }

        mediaSession.setActive(false);

        mediaSession.setPlaybackState(stateBuilder.setState(PlaybackStateCompat.STATE_STOPPED, PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1).build());
        currentState = PlaybackStateCompat.STATE_STOPPED;

        NotificationAndForegroundStatus.refreshNotificationAndForegroundStatus(currentState,baseMusicService,getMediaSession());


        baseMusicService.stopSelf();
    }



    @Override
    public void onPause() {
        // Останавливаем воспроизведение
        if (exoPlayer.getPlayWhenReady()) {
            exoPlayer.setPlayWhenReady(false);
            baseMusicService.unregisterReceiver(becomingNoisyReceiver);
        }
        // Сообщаем новое состояние
        mediaSession.setPlaybackState(
                stateBuilder.setState(PlaybackStateCompat.STATE_PAUSED,
                        PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1).build());

        currentState = PlaybackStateCompat.STATE_PAUSED;

        NotificationAndForegroundStatus.refreshNotificationAndForegroundStatus(currentState,baseMusicService,getMediaSession());

    }

    @Override
    public void onSkipToNext() {

        onStop();
        NotificationAndForegroundStatus.refreshNotificationAndForegroundStatus(currentState,baseMusicService,getMediaSession());

        prepareExoPlayerFromFileUri(myList.get(1));
//        prepareExoPlayerFromFileUri(myList.get(1));
        exoPlayer.setPlayWhenReady(true);
    }

    @Override
    public void onSkipToPrevious() {
        super.onSkipToPrevious();
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {

    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        if (playWhenReady && playbackState == Player.STATE_ENDED) {
            this.onSkipToNext();
        }
    }

    @Override
    public void onRepeatModeChanged(int repeatMode) {

    }

    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {

    }

    @Override
    public void onPositionDiscontinuity(int reason) {

    }

    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

    }

    @Override
    public void onSeekProcessed() {

    }


    private void prepareExoPlayerFromFileUri(Track s) {

            mediaSession.setMetadata(MyMediaMetadata.dateTrACK(s).build());

        exoPlayer = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(context), new DefaultTrackSelector(), new DefaultLoadControl());
        exoPlayer.setPlayWhenReady(false);
        ExtractorMediaSource media = new ExtractorMediaSource.Factory(new DefaultDataSourceFactory(context, Util.getUserAgent(context, ""))).setExtractorsFactory(new DefaultExtractorsFactory()).createMediaSource(Uri.parse(s.getUri()));
        exoPlayer.prepare(media);

    }
    @Override
    public void onCreateMediaSession(MyMediaSession myMediaSession, Context context, MyMusicService baseMusicService){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_DEFAULT_CHANNEL_ID, baseMusicService.getString(R.string.notification_channel_name), NotificationManager.IMPORTANCE_LOW);
            notificationChannel.setSound(null,null);
            NotificationManager notificationManager = (NotificationManager) baseMusicService.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);

            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build();
            audioFocusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                    .setOnAudioFocusChangeListener(audioFocusChangeListener)
                    .setAcceptsDelayedFocusGain(false)
                    .setWillPauseWhenDucked(true)
                    .setAudioAttributes(audioAttributes)
                    .build();
        }
        this.context = context;
        this.baseMusicService = baseMusicService;

        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);


        mediaSession = new MediaSessionCompat(context, " MyMusicService");
        mediaSession.setFlags(
                MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS
                        | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        mediaSession.setCallback(this);

        Intent activityIntent = new Intent(context, MainActivity.class);
        mediaSession.setSessionActivity(
                PendingIntent.getActivity(context, 0, activityIntent, 0));

        Intent mediaButtonIntent = new Intent(
                Intent.ACTION_MEDIA_BUTTON, null, context, MediaButtonReceiver.class);
        mediaSession.setMediaButtonReceiver(
                PendingIntent.getBroadcast(context, 0, mediaButtonIntent, 0));
        // Get a new or existing ViewModel from the ViewModelProvider.
        // Add an observer on the LiveData returned by getAlphabetizedWords.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.

        TrackRepository trackRepository = new TrackRepository(context.getApplicationContext());
        trackRepository.getAllTracks().observeForever(new Observer<List<Track>>() {
            @Override
            public void onChanged(@Nullable List<Track> tracks) {
                if(!tracks.isEmpty()) {
                    myList = tracks;
                    Toast.makeText(context, tracks.get(0).getTitle(), Toast.LENGTH_SHORT).show();
                    Log.d("qazwsxedcrfv", tracks.get(0).getTitle());
                    INDEX = 0;
                    setTrack(tracks.get(INDEX));
                }
            }
        });

    }
    @Override
    public void onDestroyMediaSession(){
        // Ресурсы освобождать обязательно
        if(mediaSession!=null) {
            mediaSession.release();
        }
        if(exoPlayer!=null) {
            exoPlayer.release();
        }
        context = null;
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                // Фокус предоставлен.
                // Например, был входящий звонок и фокус у нас отняли.
                // Звонок закончился, фокус выдали опять
                // и мы продолжили воспроизведение.
                this.onPlay();
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                // Фокус отняли, потому что какому-то приложению надо
                // коротко "крякнуть".
                // Например, проиграть звук уведомления или навигатору сказать
                // "Через 50 метров поворот направо".
                // В этой ситуации нам разрешено не останавливать вопроизведение,
                // но надо снизить громкость.
                // Приложение не обязано именно снижать громкость,
                // можно встать на паузу, что мы здесь и делаем.
                this.onPause();
                break;
            default:
                // Фокус совсем отняли.
                this.onPause();
                break;
        }
    }

    private AudioManager.OnAudioFocusChangeListener audioFocusChangeListener = focusChange -> {
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_GAIN:
                onPlay(); // Не очень красиво
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                onPause();
                break;
            default:
                onPause();
                break;
        }
    };

    private final BroadcastReceiver becomingNoisyReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Disconnecting headphones - stop playback
            if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
                onPause();
            }
        }
    };

    @Override
    public void onSeekTo(long pos) {
        super.onSeekTo(pos);
        exoPlayer.seekTo(pos);
    }

    public SimpleExoPlayer getExoPlayer() {
        return exoPlayer;
    }
}
