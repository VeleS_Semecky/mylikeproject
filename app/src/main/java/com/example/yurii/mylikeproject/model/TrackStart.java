package com.example.yurii.mylikeproject.model;

import android.net.Uri;

public class TrackStart {
    private String name;
    private Uri uri;
    private long duration; // in ms

    public TrackStart(String name) {
        this.name = name;
    }

    public TrackStart() {
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public TrackStart(String name, Uri uri, long duration) {

        this.name = name;
        this.uri = uri;
        this.duration = duration;
    }
}
