package com.example.yurii.mylikeproject.adapter.bean.finder;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaCodec;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.MediaController;

import com.example.yurii.mylikeproject.R;
import com.example.yurii.mylikeproject.model.Track;
import com.example.yurii.mylikeproject.model.TrackStart;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.audio.MediaCodecAudioRenderer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.mediacodec.MediaCodecSelector;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.upstream.AssetDataSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultAllocator;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;
import java.util.List;

import static android.provider.MediaStore.Audio.Media.getContentUriForPath;

@EBean
public class TrackBeanFinder {

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 99;

    @RootContext
    Context context;

    String urlQ;

    public List<Track> getTrack(){
        List<Track> arrayList = new ArrayList<>();

        int permissionCheck = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    (Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

        } else {

            ContentResolver contentResolver = context.getContentResolver();
            Uri songUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            Cursor cursor = null;
            cursor = contentResolver.query(songUri, null, null, null, null);

            assert cursor != null;
            if (songUri != null && cursor.moveToFirst()) {
                int songTitle = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
                int songArtist = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
                int songData = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);
                int songDuration = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION);

                do {
                    String currentTitle = cursor.getString(songTitle);
                    String currentArtist = cursor.getString(songArtist);
                    String currentData = cursor.getString(songData);
                    long currentDuration = cursor.getLong(songDuration);
                    arrayList.add(new Track(currentTitle,currentArtist, R.drawable.example_picture,currentData,currentDuration));
                           // new TrackStart(
//                            "Title" + currentTitle +
//                            "\n" + "Artist" + currentArtist +
//                            "\n" +
//                                    "Data" +
                               //             currentData
                                            //+
//                            "\n" +
//                                    "D" + setCorrectDuration(currentDuration)
                  //  ));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }
        return arrayList;

    }

    @Nullable
    private String setCorrectDuration(long songs_duration) {
        // TODO Auto-generated method stub
        String songDurationString;

            long seconds = songs_duration /1000;
            long minutes = seconds/60;
            seconds = seconds % 60;

            if(seconds<10){
                songDurationString = String.valueOf(minutes) + ":0" + String.valueOf(seconds);
            }else{
                songDurationString = String.valueOf(minutes) + ":" + String.valueOf(seconds);
            }
            return songDurationString;
        
    }
    public void prepareExoPlayerFromFileUri(String s){
        Uri uri = Uri.parse("/storage/emulated/0/Android/media/com.viber.voip/Notifications/viber_message.mp3");
        SimpleExoPlayer exoPlayer = ExoPlayerFactory.newSimpleInstance(new DefaultRenderersFactory(context), new DefaultTrackSelector(), new DefaultLoadControl());
        ExtractorMediaSource media = new ExtractorMediaSource.Factory(new DefaultDataSourceFactory(context, Util.getUserAgent(context, ""))).setExtractorsFactory(new DefaultExtractorsFactory()).createMediaSource(uri);
        exoPlayer.prepare(media);
        exoPlayer.setPlayWhenReady(true);

    }



}
