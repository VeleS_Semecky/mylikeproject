package com.example.yurii.mylikeproject.room;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.example.yurii.mylikeproject.model.Track;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class TrackViewModel extends AndroidViewModel {
    private TrackRepository mRepository;

    private LiveData<List<Track>> mAllTracks;

    public TrackViewModel (Application application) {
        super(application);
        mRepository = new TrackRepository(application);
        mAllTracks = mRepository.getAllTracks();
    }

    public LiveData<List<Track>> getAllTracks() { return mAllTracks; }

    public void insert(List<Track> word) { mRepository.insert(word); }
    public void delete(Track word) { mRepository.delete(word); }
}
